---
title: git, shell, ldap, etc. accounts
---

[[_TOC_]]

Note that this documentation needs work, as it overlaps with user
creation procedures, see [issue 40129](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40129).

# torproject.org Accounts #

The Tor project keeps all user information in a central LDAP database which
governs access to shell accounts, git (write) access and lets users configure
their email forwards.

It also stores group memberships which in turn affects which users can log into
which [hosts](https://db.torproject.org/machines.cgi).

This document should be consistent with the [Tor membership
policy](https://gitlab.torproject.org/tpo/community/policies/-/blob/HEAD/docs/membership.md),
in case of discrepancy between the two documents, the membership
policy overrules this document.

## <a name="ldap-or-alias">Decision tree: LDAP account or email alias?</a> ##

Here is a simple decision tree to help you decide if a new contributor
needs an LDAP account, or if an email alias will do. (All things being
equal, it's better to set people up with only an email alias if that's all
they need, since it reduces surface area which is better for security.)

### LDAP account reasons ###

Regardless of whether they are a Core Contributor:

 * Are they a maintainer for one of our official software projects, meaning
   they need to push commits (write) to one of our git repos?
   * They should get an LDAP account.
 * Do they need to access (read) a private git repo, like "dirauth-conf"?
   * They should get an LDAP account.

Are they a Core Contributor?

 * Do they want to make their own personal clones of our git repos, for example to propose patches and changes?
   * They don't need an LDAP account for just this case anymore, since gitlab can host git repos. (They are also welcome to put their personal git repos on external sites if they prefer.)
 * Do they need to log in to our servers to use our shared irc host?
   * They should get an LDAP account.
   * If they're not a Core Contributor, they should put their IRC somewhere
     else, like pastly's server.
 * Do they need to log in to our servers to maintain one of our websites or
   services?
   * An existing Core Contributor should request an LDAP account.
   * If they're not a Core Contributor, but they are a staff member who needs
     to maintain services, then Tor Project Inc should request an LDAP account.
   * If they are not a staff member, then an existing Core Contributor should
     request an LDAP account, and explain why they need access.
 * Do they need to be able to *send* email using an @torproject.org address?
   * In our 2022/2023 process of locking down email, it's increasingly necessary for people to have a full ldap account in order to deliver their tor mail to the internet properly.

See [New LDAP accounts](howto/create-a-new-user) for details.

### Email alias reasons ###

If none of the above cases apply:

 * Are they a Core Contributor?
   * An existing Core Contributor should request an email alias.
 * Are they a staff member?
   * Tor Project Inc should request an email alias.

See <a href="#aliases">Changing email aliases</a> for details.

## <a name="new-account">New LDAP accounts</a> ##

New accounts have to be sponsored by somebody who already has a torproject.org
account.  If you need an account created, please find somebody in the project
who you are working with and ask them to request an account for you.

### Step 1 ###

The sponsor will collect all required information:

 * name,
 * initial forwarding email address (the user can change that themselves later),
 * OpenPGP key fingerprint,
 * desired username.

The sponsor is responsible for verifying the information's accuracy, in
particular establishing some confidence that the key in question
actually belongs to the person that they want to have access.

The user's OpenPGP key should be available from the public keyserver network.

The sponsor will [create a ticket in GitLab](https://gitlab.torproject.org/tpo/tpa/team/-/issues/new):

 * The ticket should include a short rationale as to why the account is
   required,
 * contain all the pieces of information listed above, and
 * should be OpenPGP signed by the sponsor using the OpenPGP key we have on
   file for them.  Please enclose the OpenPGP clearsigned blob using
   `{{{` and `}}}`.

#### username policy ####

Usernames are allocated on a first-come, first-served basis. Usernames
should be checked for conflict with commonly used administrative
aliases (`root`, `abuse`, ...) or abusive names (`killall*`, ...). In
particular, the following have special meaning for various services
and should be avoided:

    root
    abuse
    arin-admin
    certmaster
    domainadmin
    hostmaster
    mailer-daemon
    postmaster
    security
    webmaster

That list, [taken from the leap
project](https://leap.se/git/leap_platform.git/tree/puppet/modules/site_postfix/manifests/mx/static_aliases.pp?id=eac3056c237d523f4786593922fe8f88eb65dff7)
is not exhaustive and your own judgement should be used to spot
possibly problematic aliases. See also those other possible lists:

 * [systemli](https://github.com/systemli/userli/blob/cc7bd7d8744df321aaea5b6289a231657dbc7643/config/reserved_names.txt)
 * [LEAP](https://leap.se/git/leap_platform.git/tree/puppet/modules/site_postfix/manifests/mx/static_aliases.pp?id=eac3056c237d523f4786593922fe8f88eb65dff7)
 * [immerda](https://git.immerda.ch/iapi/tree/lib/iapi/helpers/forbidden_aliases.rb)

### Step n+1 ###

Once the request has been filed it will be reviewed by Roger or Nick
and either approved or rejected.

If the board indicates their assent, the sysadmin team will then create the
account as requested.

## <a name="retiring-account">Retiring accounts</a> ##

If you won't be using your LDAP account for a while, it's good security
hygiene to have it disabled. Disabling an LDAP account is a simple
operation, and reenabling an account is also simple, so we shouldn't be
shy about disabling accounts when people stop needing them.

To simplify the review process for disable requests, and because disabling
by mistake has less impact than creating a new LDAP account by mistake, the
policy here is "any two of {Roger, Nick, Shari, Isabela, Erin, Damian}
are sufficient to confirm a disable request."

(When we disable an LDAP account, we should be sure to either realize
and accept that email forwarding for the person will stop working too,
or add a new line in the email alias so email keeps working.)

## <a name="get-access">Getting added to an existing group/Getting access to a specific host</a> ##

Almost all privileges in our infrastructure, such as account on a particular
host, sudo access to a role account, or write permissions to a specific
directory, come from group memberships.

To know which group has access to an specific host, FIXME.

To get added to some unix group, it has to be requested by a member of
that group.
This member has to [create a new ticket in GitLab](https://gitlab.torproject.org/tpo/tpa/team/-/issues/new),
OpenPGP-signed (as above in the new account creation section),
requesting who to add to the group.

If a new group needs to be created, FIXME.

The reasons why a new group might need to be created are: FIXME.

Should the group be orphaned or have no remaining active members, the
same set of people who can approve new account requests can request
you be added.

To find out who is on a specific group you can ssh to perdulce:

    ssh perdulce.torproject.org

Then you can run:

    getent group

See also: the `"Host specific passwords"` section below

## <a name="aliases">Changing email aliases</a> ##

Create a ticket specifying the alias, the new address to add, and a
brief motivation for the change.

For specifics, see the "The sponsor will create a ticket" section above.

### <a name="new-aliases">Adding a new email alias</a> ###

#### Personal Email Aliases ####

Tor Project Inc can request new email aliases for staff.

An existing Core Contributor can request new email aliases for new Core
Contributors.

#### Group Email Aliases ####

Tor Project Inc and Core Contributors can request group email aliases for new
functions or projects.

### <a name="existing-aliases">Getting added to an existing email alias</a> ###

Similar to being added to an LDAP group, the right way to get added
to an existing email alias is by getting somebody who is already on
that alias to file a ticket asking for you to be added.

## <a name="password-reset">Changing/Resetting your passwords</a> ##

### LDAP ###

If you've lost your LDAP password, you can request that a new one be
generated. This is done by sending the phrase "Please change my Debian
password" to chpasswd@db.torproject.org. The phrase is required to prevent the
daemon from triggering on arbitrary signed email. The best way to invoke this
feature is with

    echo "Please change my Debian password" | gpg --armor --sign | mail chpasswd@db.torproject.org

After validating the request the daemon will generate a new random password,
set it in the directory and respond with an encrypted message containing the
new password. This new password can then be used to
[login](https://db.torproject.org/login.html) (click the `"Update my info"`
button), and use the `"Change password"` fields to create a new LDAP
password.

Note that LDAP (and sudo passwords, below) changes are not
instantaneous: they can take between 5 to 8 minutes to propagate to
any given host.

More specifically, the password files are generated on the master LDAP
server every five minutes, starting at the third minute of the hour,
with a cron schedule like this:

     3,8,13,18,23,28,33,38,43,48,53,58

Then those files are synchronized on a more standard 5 minutes
schedule to all hosts.

There are also delays involved in the mail loop, of course.

<a name="sudo" />

### Host specific passwords / sudo passwords ###

Your LDAP password can *not* be used to authenticate to `sudo` on
servers. It can only allow to log you in through SSH, but you need a
*different* password to get `sudo` access, which we call the "sudo
password".

To set the sudo password:

 1. go to the [user management website](https://db.torproject.org/login.html)
 2. pick "Update my info"
 3. set a new (strong) sudo password

If you want, you can set a password that works for all the hosts that
are managed by torproject-admin, by using the "wildcard ("*").
Alternatively, or additionally, you can have per-host sudo passwords
-- just select the appropriate host in the pull-down box.

Once set on the web interface, you will have to confirm the new
settings by sending a signed challenge to the mail interface. The
challenge is a single line, without line breaks, provided by the web
interface. With the challenge first you will need to produce an
openpgp signature:

    echo 'confirm sudopassword ...' | gpg --armor --sign

With it you can compose an email to changes@db.torproject.org, sending
the challenge in the body followed by the openpgp signature.

Note that setting a sudo password will only enable you to use sudo to
configured accounts on configured hosts. Consult the output of "sudo
-l" if you don't know what you may do. (If you don't know, chances are
you don't need to nor can use sudo.)

Do mind the delays in LDAP and sudo passwords change, mentioned in the
previous section.

## <a name="key-rollover">Changing/Updating your OpenPGP key</a> ##

If you are planning on migrating to a new OpenPGP key and you also want to
change your key in LDAP, or if you just want to update the copy of your key
we have on file, you need to [create a new ticket in GitLab](https://gitlab.torproject.org/tpo/tpa/team/-/issues/new):

 * The ticket should include your username, your old OpenPGP fingerprint
   and your new OpenPGP fingerprint (if you're changing keys).
 * The ticket should be OpenPGP signed with your OpenPGP key that is currently
   stored in LDAP.

### Revoked or lost old key ###

If you already revoked or lost your old OpenPGP key and you migrated to a
new one before updating LDAP, you need to find a sponsor to create a
ticket for you. The sponsor should [create a new ticket in GitLab](https://gitlab.torproject.org/tpo/tpa/team/-/issues/new):

 * The ticket should include your username, your old OpenPGP fingerprint
   and your new OpenPGP fingerprint.
 * Your OpenPGP key needs to be on a public keyserver and be signed by at
   least one Tor person other than your sponsor.
 * The ticket should be OpenPGP signed with the current valid OpenPGP key of
   your sponsor.

### Actually updating the keyring ###

See the [new-user HOWTO](../howto/create-a-new-user).
