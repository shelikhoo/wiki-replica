[[_TOC_]]

# Roll call: who's there and emergencies

anarcat, kez, lavamind

# Roadmap review

Everything postponed, to focus on fixing alerts and preparing for the
holidays. A discussion of the deluge and a list of postponed issues
has been documented in [issue 41411][].

[issue 41411]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41411

# Holidays

We've looked at the coming holidays and allocated schedules for
rotation, documented in the "TPA" Nextcloud calendar. A handoff should
occur on December 30th.

# Next meeting

Planned for January 15th, when we'll hopefully be able to schedule a
roadmap for the coming 2024 year.

Anarcat has ordered 2024 to be better than 2023 or else.

# Metrics of the month

 * hosts in Puppet: 88, LDAP: 88, Prometheus exporters: 165
 * number of Apache servers monitored: 35, hits per second: 602
 * number of self-hosted nameservers: 6, mail servers: 10
 * pending upgrades: 0, reboots: 36
 * average load: 0.56, memory available: 3.40 TiB/4.80 TiB, running
   processes: 420
 * disk free/total: 68.51 TiB/131.80 TiB
 * bytes sent: 366.92 MB/s, received: 242.77 MB/s
 * planned bookworm upgrades completion date: 2024-08-03
 * [GitLab tickets][]: 206 tickets including...
   * open: 0
   * icebox: 159
   * backlog: 21
   * next: 11
   * doing: 5
   * needs review: 4
   * (closed: 3383)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at
https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bookworm/

Now also available as the main Grafana dashboard. Head to
<https://grafana.torproject.org/>, change the time period to 30 days,
and wait a while for results to render.
