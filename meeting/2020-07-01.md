[[_TOC_]]

# Roll call: who's there and emergencies

Hiro and anarcat present in the meeting. Quick chat by mumble do to a
check-in and resolve some issues with the installer to setup
fsn-node-07 and check overall priorities.

# Roadmap review

We looked at the [issue board][] which excludes GitLab, because that board was processed in the
gitlab meeting yesterday.

[issue board]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

We went through the tickets and did some triage, moving some tickets
from `Open` to `Backlog` and some tickets into `Next`. anarcat has no
tickets left in `Backlog` because he's going away for a two months
leave. hiro will review her ticket priorities within the week.

## GitLab workflow changes

We tried to get used to the new GitLab workflow.

We decided on using the "Next" label to follow the global @tpo
convention, although we have not adopted the "Icebox" label yet. The
gitlab policy was changed to:

> Issues first land into a "triage" queue (`Open`), then get assigned to
> a specific milestone as the ticket gets planned. We use the `Backlog`,
> `Next`, and `Doing` of the global "TPO" group board labels. With the
> `Open` and `Closed` list, this gives us the following policy:
>
> * `Open`: untriaged ticket, "ice box"
> * `Backlog`: planned work
> * `Next`: work to be done in the next iteration or "sprint"
>   (e.g. currently a month)
> * `Doing`: work being done right now (generally during the day or
>   week)
> * `Closed`: completed work
>
> That list can be adjusted in the future without formally reviewing
> this policy.
>
> Priority of items in the lists are determined by the *order* of items
> in the stack. Tickets should *not* stay in the `Next` or `Doing` lists
> forever and should instead actively be closed or moved back into the
> `Open` or `Backlog` board.

Note that those policies are still being discussed in the GitLab
project, see [issue 28][] for details.

 [issue 28]: https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/28

## Exciting work that happened in June

 * [Trac migrated to GitLab][]
 * [TPA wiki][] migrated to GitLab
 * kvm4 and kvm5 were retired, signaling the end of the "libvirt/KVM"
   era of our virtual hosting: all critical services now live in
   Ganeti
 * lots of buster upgrades happened

 [Trac migrated to GitLab]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/gitlab/#migration
 [TPA wiki]: https://gitlab.torproject.org/tpo/tpa/team/

# Hand-off

During the mumble check-in, hiro and anarcat established there was not
any urgent issue requiring training or work.

anarcat will continue working on the documentation tickets as much as
he can before leaving ([Puppet][], [LDAP][], [static mirrors][]) but
will otherwise significantly reduce his work schedule.

[Puppet]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40024
[LDAP]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/34426
[static mirrors]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/34436

# Other discussions

No other discussions were held.

# Next meeting

No next meeting is currently planned, but the next one should normally
be held on Wednesday August 5th, according to our normal schedule.

# Metrics of the month

 * hosts in Puppet: 72, LDAP: 75, Prometheus exporters: 126
 * number of apache servers monitored: 29, hits per second: 176
 * number of nginx servers: 2, hits per second: 2, hit ratio: 0.87
 * number of self-hosted nameservers: 6, mail servers: 12
 * pending upgrades: 1, reboots: 0
 * average load: 0.67, memory available: 271.44 GiB/871.88 GiB, running processes: 400
 * bytes sent: 211.50 MB/s, received: 113.43 MB/s
 * GitLab tickets: 171 issues including...
   * open: 125
   * backlog: 26
   * next: 13
   * doing: 7
   * (closed: 2075)
 * number of Trac tickets migrated to GitLab: 32401
 * last Trac ticket ID created: 34451
 * planned buster upgrades completion date: 2020-08-11

Only 3 nodes left to upgrade to buster: troodi (trac), gayi (svn) and rude (RT).

Upgrade prediction graph still lives at
https://help.torproject.org/tsa/howto/upgrades/

Now also available as the main Grafana dashboard. Head to
<https://grafana.torproject.org/>, change the time period to 30 days,
and wait a while for results to render.

