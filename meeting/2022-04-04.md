# Roll call: who's there and emergencies

First few minutes of the meeting were spent dealing with blocking
issues with office.com, which ultimately led to disabling sender
verification. See [tpo/tpa/team#40627][] for details.

[tpo/tpa/team#40627]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40627

Present: anarcat, gaba, kez, lavamind, linus.

# Roadmap / OKR review

We reviewed our two roadmaps:

 * [sysadmin OKRs][]
 * [web OKRs][]

[sysadmin OKRs]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2022
[web OKRs]: https://gitlab.torproject.org/tpo/web/team/-/wikis/roadmap/2022

## TPA OKRs

We didn't do much in the TPA roadmap, unfortunately. Hopefully this
week will get us started with the bullseye upgrades, and some
initiatives have been *started* but it looks like we will probably not
fulfill most (let alone all) of our objectives for the roadmap inside
TPA.

## web OKRs

More progress was done on the web side of things:

 * donate: lektor frontend needs to be cleaned up, some of the
   settings are still set in react instead of with lektor's
   `contents.lr`. Vanilla JS rewrite mostly complete, possibly enough
   that the rest can be outsourced. Still no .onion since production
   is running the react version (doesn't run in tbb) and .onion might
   also break on the backend. We also don't have an HTTPS certificate
   for the backend!

 * translators: good progress on this front, build time blocking on
   the i18n plugin status ([TPA-RFC-16][]), stuck on Python 3.8 land,
   we are also going to make changes to the workflow to allow
   developers to merge MRs (but not push)

 * documentation: removed some of the old docs, dev.tpo for Q2?

The TPA-RFC-16 proposal (rewriting the lektor-i18n plugin) was
discussed a little more in depth. We will get more details about the
problems kez found with the other CMSes and a rough comparison of the
time that would be required to migrate to another CMS vs rewriting the
plugin. See [tpo/web/team#28][] for details.

 [TPA-RFC-16]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-16-replacing-lektor-i18n-plugin
 [tpo/web/team#28]: https://gitlab.torproject.org/tpo/web/team/-/issues/28

# Dashboard review

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

Skipped for lack of time

# Holidays

Skipped for lack of time

# Other discussions

Skipped for lack of time

# Next meeting

May 2nd, same time. We should discuss phase 3 of bullseye upgrades
next meeting, so that we can make a decision about the stickiest
problems like Icinga 2 vs Prometheus, Schleuder, Mailman, Puppet 6/7,
etc.

# Metrics of the month

 * hosts in Puppet: 91, LDAP: 91, Prometheus exporters: 149
 * number of Apache servers monitored: 26, hits per second: 314
 * number of self-hosted nameservers: 6, mail servers: 8
 * pending upgrades: 1, reboots: 23
 * average load: 3.62, memory available: 4.58 TiB/5.70 TiB, running processes: 749
 * disk free/total: 29.72 TiB/85.33 TiB
 * bytes sent: 382.46 MB/s, received: 244.51 MB/s
 * planned bullseye upgrades completion date: 2025-01-30
 * [GitLab tickets][]: 185 tickets including...
   * open: 0
   * icebox: 157
   * backlog: 12
   * next: 8
   * doing: 5
   * needs review: 1
   * needs information: 2
   * (closed: 2680)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at
https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bullseye/

Now also available as the main Grafana dashboard. Head to
<https://grafana.torproject.org/>, change the time period to 30 days,
and wait a while for results to render.
