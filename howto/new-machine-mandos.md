Mandos is a means to give LUKS keys to machines that want to boot but have an encrypted rootfs.

Here's how you add a new client to our setup:

 1. add a new key to the LUKS partition and prepare mandos snippet:

         echo 'encrypted (root/lvm/..) device (e.g. /dev/sda2 or /dev/mb/pv_nvme): ' && read DEVICE &&
         apt install -y haveged mandos-client &&
         (grep 116.203.128.207 /etc/mandos/plugin-runner.conf || echo '--options-for=mandos-client:--connect=116.203.128.207:16283' | tee -a /etc/mandos/plugin-runner.conf) &&
         umask 077 &&
         t=`tempfile` &&
         dd if=/dev/random bs=1 count=128 of="$t" &&
         cryptsetup luksAddKey $DEVICE "$t" &&
         mandos-keygen --passfile "$t"

    If you are unsure which device is LUKS installed on, you could
    check it with:
    
        lsblk --fs

 2. add the `roles::fde` class to new host in Puppet and run puppet
    there:
    
        puppet agent -t

    If the class was already applied on a previous Puppet run, ensure
    the initramfs image is updated at this point:

        update-initramfs -u

 3. on the mandos server, add the output of `mandos-keygen` from above
    to `/etc/mandos/clients.conf` and restart the service:
 
        service mandos restart

 4. on the mandos server, update the firewall after you added the host
    to ldap:

        puppet agent -t

 5. on the mandos server, enable the node:
 
        mandos-ctl --enable $FQDN

 4. reboot the new host to test unlocking

TODO: Mandos setups should be automatic, see [issue 40096](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40096).
