[[_TOC_]]

# How to

This page contains the procedure to rename a host. It hasn't been tested very
much, so proceed with caution.

## Remove host from Puppet

Start by stopping the `puppet-run` timer and disabling Puppet on the machine:

    systemctl stop puppet.timer && \
    puppet agent --disable "renaming in progress"

Then, in `tor-puppet`, remove references to the host. At the very least the
node's classification yaml should be removed for `hiera-enc/nodes`.

Revoke its certificates from the Puppet server using the retirement script:

    retire -H foo.torproject.org revoke-puppet

## Change the hostname

On the host being renamed, change the hostname:

    hostnamectl set-hostname bar.torproject.org && \
    sed -i 's/foo/bar/g' /etc/hosts

Then adjust the SSH host keys. Generating new keys isn't mandatory:

    sed -i 's/foo/bar/' /etc/ssh/ssh_host_*.pub

We also need to fix the `thishost` symlink in ud-ldap data:

    ud-replicate
    cd /var/lib/misc && ln -sf bar.torproject.org thishost
    rm -rf foo.torproject.org

## Rename the machine in the infrastructure

### Ganeti

    ganeti-instance rename foo.torproject.org bar.torproject.org

### LDAP

Run a search/replace with the old and new hostname in the host's stanza.

### Mandos

We need to let the mandos server know about the new hostname:

    sed -i 's/foo/bar/' /etc/mandos/clients.conf && \
    systemctl restart mandos.service

### DNS

Both forward and reverse DNS should be adjusted to use the new hostname.

### DNSWL

### Nagios

### External hoster platform

If the host is a machine host at Hetzner or another provider, the name should
be changed there as well.

## Re-bootstrap Puppet on the host

Now the host is ready to be added back to Puppet. A new certificate will be
generated in this step.

    puppet agent --enable && \
    cd ~ && \
    test -d fabric-tasks || git clone https://gitlab.torproject.org/tpo/tpa/fabric-tasks.git && \
    cd fabric-tasks && \
    ./installer/puppet-bootstrap-client

## Schedule backups removal

This will schedule the removal of backups under the old hostname:

    retire -H foo.torproject.org remove-backups

## Adjust documentation

Adjust documentation that may refer to the old hostname, including the
tor-passwords, the wiki and the Tor "VM Hosts" spreadsheet.
