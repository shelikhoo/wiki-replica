---
title: TPA-RFC-46: GitLab 2FA
---

[[_TOC_]]

Summary: enforce 2FA in the TPA group in GitLab on Tuesday, 2 day
grace period

# Background

GitLab groups have a setting to force users in the group to use 2FA
authentication. The actual setting is labeled "All users in this group
must set up two-factor authentication".

It's not exactly clear what happens when a user is already a member and
the setting is enabled, but it is assumed it will keep the user from
accessing the group.

# Proposal

Enable the "enforce 2FA" setting for the `tpo/tpa` group in GitLab.

## Affected users

All users but one 

# Approval

Needs approval from TPA.

# Deadline

The setting will be ticked on Tuesday January 17th, with a 48h grace
period, which means that users without 2FA will not be able to access
the group with privileges on Thursday January 19th.

# Status

This proposal is currently in the `standard` state.

# References

 * [GitLab documentation about the feature][]
 * discussion ticket: [tpo/tpa/team#40892][]

[GitLab documentation about the feature]: https://docs.gitlab.com/ee/security/two_factor_authentication.html#enforce-2fa-for-all-users-in-a-group

[tpo/tpa/team#40892]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40892
