---
title: TPA-RFC-28: alphabetical triage star of the week
---

[[_TOC_]]

Summary: sort the triage star of the week alphabetically

# Background

We currently refer to the [November 1 2021 meeting](meeting/2021-11-01) whenever we
fail to remember the order of those names from one week to the
next. That is a waste of time and should be easier.

# Proposal

Make the order alphabetical, based on the IRC nicknames. 

This is actually a patch to TPA-RFC-2, as detailed in this MR:

https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/merge_requests/29

Therefore, when the MR is merged, this proposal will become obsolete.

# Examples or Personas

Example: anarcat, kez, lavamind.

Counter-example: antoine, jerome, kez.

This also means that this week is anarcat's turn instead of kez. Kez
will take next week.

# Approval

TPA.

# Deadline

This will be approved in a week unless there are any objections.

# Status

This proposal is currently in the `obsolete` state.

# References

See [policy/tpa-rfc-2-support](policy/tpa-rfc-2-support).
