---
title: TPA-RFC-7: root access
---

Summary: who should get administrator privileges, where, how and when?
How do those get revoked?

# Background

Administrator privileges on TPO servers is reserved to a small group,
currently the "members of TPA", a loose group of sysadmins with no
clearly defined admission or exit rules.

There are multiple possible access levels, often conflated:

 1. `root` on servers: user has access to the `root` user on some or
    all UNIX servers, either because they know the password, or have
    their SSH keys authorized to the root user (through Puppet, in the
    `profile::admins::keys` Hiera field)
 2. `sudo` to root: user has access to the `root` user through `sudo`,
    using their `sudoPassword` defined in LDAP. Puppet access: by
    virtue of being able to push to the Puppet git repository, an
    admin necessarily gets `root` access everywhere, because Puppet
    runs as root everywhere
 4. LDAP admin: a user member of the `adm` group in LDAP also gets
    access everywhere through `sudo`, but also through being able to
    impersonate or modify other users in LDAP (although that requires
    shell access to the LDAP server, which normally requires root)
 5. password manager access: a user's OpenPGP encryption key is added
    to the `tor-passwords.git` repository, which grants access to
    various administrative sites, root passwords and cryptographic
    keys

This approach is currently all-or-nothing: either a user has access to
all of the above, or nothing. That list might not be exhaustive. It
certainly does not include the service admin access level.

The current list of known administrators is:

 * anarcat
 * arma (not in root's authorized keys)
 * hiro
 * linus
 * qbi (not in root's authorized keys)
 * weasel

Unless otherwise mentioned, those users have all the access mentioned
above.

Another issue that currently happens is the problems service admins
(which do not have root access) have in managing some services. In
particular, Schleuder and GitLab service admins have had trouble
debugging problems with their service because they do not have the
necessary access levels to restart their service, edit configuration
file or install packages.

# Proposal

This proposal aims at clarifying the current policy, but also
introduces an exception for service admins to be able to become root
on the servers they manage (and only those). It also tries to define a
security policy for access tokens, as well as admission and revocation
policies.

In general, the spirit of the proposal is to bring more flexibility
with what changes we allow on servers to the TPA team. We want to help
teams host their servers with us but that also comes with the
understanding that we need the capacity (in terms of staff and
hardware resources) to do so as well.

## Scope

This policy complements the [Tor Core Membership policy][] but
concerns only membership to the TPA team and access to servers.

[Tor Core Membership policy]: https://gitlab.torproject.org/tpo/community/policies/-/raw/HEAD/docs/membership.md

## Access levels

Members of TPA SHOULD have all access levels defined above.

Service admins MAY have some access to some servers. In general, they
MUST have `sudo` access to a role account to manage their own
service. They MAY be granted LIMITED `root` access (through `sudo`)
only on the server(s) which host their service, but this should be
granted only if there are no other technical way to implement the
service.

In general, service admins SHOULD use their `root` access in
"read-only" mode for debugging, as much as possible. Any "write"
changes MUST be documented, either in a ticket or in an email to the
TPA team (if the ticket system is down). Common problems and their
resolutions SHOULD be documented in the [service documentation
page](service).

Service admins are responsible for any breakage they cause to systems
while they use elevated privileges.

## Token security

Extreme care should be taken with private keys: authentication keys
(like SSH keys or OpenPGP encryption keys) MUST be password-protected
and ideally SHOULD reside on hardware tokens, or at least SHOULD be
stored offline.

## Admission and revocation

Service admins and system administrators are granted access through a
vetting process by which an existing administrator requests access for
the new administrator. This is currently done by opening a ticket in
the issue tracker with an OpenPGP-signed message, but that is
considered an implementation detail as far as this procedure is
concerned.

A service admin or system administrator MUST be part of the "Core
team" as defined by the [Tor Core Membership policy][] to keep their
privileges.

Access revocation should follow the termination procedures in the [Tor
Core Membership policy][], which, at the time of writing, establish
three methods for ending the membership:

 1. voluntary: members can resign by sending an email to the team
 2. inactivity: members accesses can be revoked after 6 months of
    inactivity, after consent from the member or a decision of the
    community team
 3. involuntary: a member can be expelled following a decision of the
    community team and membership status can be temporarily revoked in
    the case of a serious problem while the community team makes a
    decision

# Examples

 * ahf should have root access on the GitLab server, which would have
   helped diagnosing the [problem following the 13.5 upgrade](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40074)
 * the `onionperf` services were setup outside of TPA because they
   required custom `iptables` rules, which wasn't allowed before but
   would be allowed under this policy: TPA would deploy the requested
   rule or, if they were dynamic, allow write access to the
   configuration somehow

# Counter examples

 * service admins MUST NOT be granted root access on all servers
 * dgoulet should have root access on the Schleuder server but cannot
   have it right now because Schleuder is on a server that also hosts
   the main email and mailing lists services
 * service admins do not need root access to the monitoring server to
   have their services monitored: they can ask TPA to setup a scrape
   or we can configure a server which would allow collaboration on the
   monitoring configuration ([issue 40089][])

[issue 40089]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40089

# Deadline

This proposal will be adopted on December 7th 2020, unless there are
objections.

# Status

This proposal is currently in the `standard` state.

<!--  LocalWords:  TPO TPA OpenPGP Hiera LDAP dgoulet GitLab ahf arma
 -->
<!--  LocalWords:  Schleuder cryptographic anarcat hiro linus qbi
 -->
