---
title: TPA-RFC-60: GitLab 2-factor authentication enforcement
---

[[_TOC_]]

Summary: This RFC seeks to enable 2-factor authentication (2fa) enforcement on
the GitLab `tpo` group and subgroups. If your Tor Project GitLab account
already has 2fa enabled, you will be unaffected by this policy.

# Background

On January 11 2024, GitLab [released a security update][] to address a
vulnerability ([CVE-2023-7028][]) allowing malicious actors to take over a
GitLab account using the password reset mechanism. Our instance was immediately
updated and subsequently [audited][] for exploits of this flaw and no evidence
of compromise was found.

Accounts configured for 2-factor authentication were never susceptible to this
vulnerability.

[released a security update]: https://about.gitlab.com/releases/2024/01/11/critical-security-release-gitlab-16-7-2-released/#account-takeover-via-password-reset-without-user-interactions
[CVE-2023-7028]: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-7028
[audited]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41470

# Proposal

Reinforce the security of our GitLab instance by enforcing 2-factor
authentication for all project members under the `tpo` namespace.

This means changing these two options under the groups Settings / Permissions
and group features section:

- Check `All users in this group must set up two-factor authentication`
- Uncheck `Subgroups can set up their own two-factor authentication rules`

## Goals

Improve the security of privileged GitLab contributor accounts.

## Scope

All GitLab accounts that are members of projects under the `tpo` namespace,
including projects in sub-groups (eg. `tpo/web/tpo`).

## Affected users

The vast majority of affects users already have 2-factor authentication
enabled.  This will affect those that haven't yet set it up, and accounts that
may be created and granted privileges in the future.

An automated listing of `tpo` sub-group and sub-project members not being
available, a manual count of users without 2fa enabled was done for all direct
subgroups of `tpo`: 17 accounts were found with 2fa disabled.

# Approval

The GitLab administrators must approve this change.

# Deadline

Enforcement is to be enabled on Monday, February 5th, followed by a 48 hour
grace period.

# Status

This proposal is currently in the `standard` state.

# References

See discussion ticket at https://gitlab.torproject.org/tpo/tpa/team/-/issues/41473

The GitLab feature allowing 2-factor authentication enforcement for groups is
documented at https://gitlab.torproject.org/help/security/two_factor_authentication#enforce-2fa-for-all-users-in-a-group
