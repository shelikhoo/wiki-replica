---
title: TPA-RFC-9: "proposed" status and small process changes
---

Summary: add a `proposed` state to the TPA-RFC process, clarify the
modification workflow, the `obsolete` state, and state changes.

# Background

The [TPA-RFC-1 policy](policy/tpa-rfc-1-policy) established a workflow to bring
proposals inside the team, but doesn't clearly distinguish between a
proposal that's currently being written (a `draft`) and a proposal
that's actually been proposed (also a `draft` right now).

Also, it's not clear how existing proposal can be easily changed
without having too many "standards" that pile up on top of each
other. For example, this proposal is technically necessary to change
TPA-RFC-1, yet if the old process was followed, it would remain
"standard" forever. A more logical state would be "obsolete" as soon
as it is adopted, and the relevant changes be made directly in the
original proposal.

The original idea of this process was to keep the text of the original
RFC static and never-changing. In practice, this is really annoying:
it means duplicating the RFCs and changing identifiers all the
time. Back when the original RFC process was established by the IETF,
that made sense: there was no version control and duplicating proposal
made sense. But now it seems like a better idea to allow a bit more
flexibility in that regard.

# Proposal

 1. introduce a new `proposed` state into TPA-RFC-1, which is the next
    state after `draft`. a RFC gets into the `proposed` state when it
    is officially communicated to other team members, with a deadline
 2. allow previous RFCs to be modified explicitly, and make the status
    of the modifying RFC be "obsolete" as soon as it is adopted
 3. make a nice graph of the state transitions
 4. be more generous with the `obsolete` state: implemented decisions
    might be marked as obsolete when it's no longer relevant to keep
    them as a running policy

## Scope

This only affects workflow of proposals inside TPA and
obsessive-compulsive process nerds.

## Actual proposed diff

```
modified   policy/tpa-rfc-1-policy.md
@@ -70,12 +70,12 @@ and a unique, incremental number. This proposal, for example, is
 
 ## Process
 
-When the proposal is first written and the notification is sent, the
-proposal is considered a `draft`. It then enters a discussion period
-during which changes can be proposed and objections can be
-raised. That period ranges from 2 business days and two weeks and is
-picked in good faith by the proposer based on the urgency of the
-changes proposed.
+When the proposal is first written, the proposal is considered a
+`draft`. When a notification is sent, the proposal is in the
+`proposed` state and then enters a discussion period during which
+changes can be proposed and objections can be raised. That period
+ranges from 2 business days and two weeks and is picked in good faith
+by the proposer based on the urgency of the changes proposed.
 
 Objections must be formulated constructively and justified with
 reasonable technical or social explanations. The goal of this step is
@@ -91,26 +91,38 @@ mitigate those problems.
 A proposal is in any of the following states:
 
  1. `draft`
+ 2. `proposed`
  2. `standard`
  3. `rejected`
  4. `obsolete`
 
+Here is a graph of the possible state transitions:
+
+\![workflow.png](workflow.png)
+
 Once the discussion period has passed and no objection is raised, the
-`draft` is adopted and becomes a `standard`.
+`proposed` RFC is adopted and becomes a `standard`.
 
 If objections are raised and no solution is found, the proposal is
 `rejected`.
 
 Some policies can be completely overridden using the current policy
 process, including this policy, in which case the old policy because
-`obsolete`.
-
-Note that a policy can be modified by later proposals. The older
-policy is modified only when the new one becomes `standard`. For
-example, say `TPA-RFC-X` proposes changes to a previous `TPA-RFC-N`
-proposal. In that case, the text of `TPA-RFC-N` would be modified when
-and only if `TPA-RFC-X` becomes a `standard`. The older `TPA-RFC-N`
-would also stay a `standard`.
+`obsolete`. Old, one-time decisions can also be marked as `obsolete`
+when it's clear they do not need to be listed in the main policy
+standards.
+
+A policy can also be **modified** (instead of **overridden** by later
+proposals or decisions taking in meetings, in which case it stays a
+`standard`.
+
+For TPA-RFC process changes, the older policy is modified only when
+the new one becomes `standard`. For example, say `TPA-RFC-X` proposes
+changes to a previous `TPA-RFC-N` proposal. In that case, the text of
+`TPA-RFC-N` would be modified when and only if `TPA-RFC-X` is adopted
+as a `standard`. The older `TPA-RFC-N` would also stay a `standard`,
+although the *newer* `TPA-RFC-X` would actually become `obsolete` as
+soon as the older `TPA-RFC-N` is modified.
 
 # Examples
 
@@ -134,6 +146,12 @@ Counter examples:
  * picking a different hardware configuration for the new ganeti node
    (process wasn't documented explicitly, we accept honest mistakes)
 
+Examples of obsolete proposals:
+
+ * [TPA-RFC-4: prometheus disk](tpa-rfc-4-prometheus-disk) was marked as obsolete a while
+   after the change was implemented.
+
+
 # Deadline
 
 Considering that the proposal was discussed and informally approved at
```

## Workflow graph

The following graph will also be attached to TPA-RFC-1:

![workflow.png](workflow.png)

# Examples

Examples:

 * [TPA-RFC-4: prometheus disk](tpa-rfc-4-prometheus-disk) was marked as obsolete when the
   change was implemented.
 * this proposal will be marked as obsolete as soon as the changes are
   implemented in TPA-RFC-1
 * this proposal would be in the `proposed` state if it was already adopted

# Deadline

December 17th.

# Status

This proposal is currently in the `obsolete` state.

# References

See [policy/tpa-rfc-1-policy](policy/tpa-rfc-1-policy).
