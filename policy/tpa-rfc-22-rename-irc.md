---
title: TPA-RFC-22: Rename TPA IRC channel and Matrix bridge
---

[[_TOC_]]

Summary: rename `#tpo-admin` to `#tor-admin` and add to the Matrix/IRC bridge.

# Background

It's unclear exactly why, but the IRC channel where TPA people meet
and offer realtime support for people is called `#tpo-admin`,
presumably for "torproject.org administrators". All other Tor-related
channels are named with a `#tor-` prefix (e.g. `#tor`, `#tor-dev`,
`#tor-project`, etc).

# Proposal

Let's follow the naming convention and rename the channel
`#tor-admin`. While we're there, add it to the Matrix bridge so people
can find us there as well.

The old channel will forward to the new one with the magic
`+f#tor-admin` (Forward) and `+l1` (limit to 1), and have ChanServ
occupy the old channel. Documentation in the wiki will be updated to
match, and the new channel settings will be modified to match the old
one.

Update: OFTC doesn't actually support the `+f` mode nor for ChanServ
to "guard" a channel. The channel will be set

# Alternatives considered

Other ideas include:

 * `#tor-sysadmins` - too long, needlessly different from `#tpo-admin`
 * `#tor-support` - too generic, `#tor` is the support channel
 * `#tor-tpa` - too obscure?
 * `#tor-sre` - would love to do SRE, but we're not really there yet

# Approval

TPA.

# Deadline

One week from sending to tpa-team.

# Status

This proposal is currently in the `standard` state.

# References

At least those pages will need an update:

 * [howto/irc](howto/irc)
 * [support](support)

... but we'll grep for that pattern everywhere just in case.

Work on this proposal is tracked in [tpo/tpa/team#40731][].

 [tpo/tpa/team#40731]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40731
