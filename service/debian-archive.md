# deb.torproject.org

* Host: palmeri.torproject.org
* All the stuff: `/srv/deb.torproject.org`
* LDAP group: tordeb

The repository is managed using [[https://wikitech.wikimedia.org/wiki/Reprepro|reprepro]].

The primary purpose of this repository is to provide a repository with experimental and nightly tor packages. Additionally, it provides up-to-date backports for Debian and Ubuntu suites.

Some backports have been maintained here for other packages, though it is preferred that this happens in Debian proper. Packages that are not at least available in Debian testing will not be considered for inclusion in this repository.

## Packages

The following packages are available in the repository:

### deb.torproject.org-keyring

* Maintainer: weasel
* Suites: all regular non-experimental suites

It contains the archive signing key.

### tor

* Maintainer: weasel
* Suites: all regular suites, including experimental suites

Builds two binary packages: tor and tor-geoipdb.

## Processes

### Removing a package from deb.torproject.org

```
tordeb@palmeri:/srv/deb.torproject.org$ bin/show-all-packages | grep $PACKAGETOREMOVE
tordeb@palmeri:/srv/deb.torproject.org$ reprepro -b /srv/deb.torproject.org/reprepro remove $RELEVANTSUITE $PACKCAGETOREMOVE
```

Packages are probably in more than one suite. Run `show-all-packages` again at the end to make sure you got them all.

## Alternatives

You do not need to use deb.torproject.org to be able to make Debian packages available for installation using apt! You could instead host a Debian repository in your people.torproject.org webspace, or alongside releases at dist.torproject.org.
