Tor-weather is a web service that alerts relay operators about issues with their relays. It runs on the host weather-01.

[[_TOC_]]

<!-- note: this template was designed based on multiple sources: -->
<!-- https://www.divio.com/blog/documentation/ -->
<!-- http://opsreportcard.com/section/9-->
<!-- http://opsreportcard.com/section/11 -->
<!-- comments like this one should be removed on instantiation -->

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Pager playbook

<!-- information about common errors from the monitoring system and -->
<!-- how to deal with them. this should be easy to follow: think of -->
<!-- your future self, in a stressful situation, tired and hungry. -->

## Disaster recovery

<!-- what to do if all goes to hell. e.g. restore from backups? -->
<!-- rebuild from scratch? not necessarily those procedures (e.g. see -->
<!-- "Installation" below but some pointers. -->

# Reference

<!-- this section is a more in-depth review of how this service works, -->
<!-- how it's setup. day-to-day operation should be covered in -->
<!-- tutorial or how-to, this is more in-depth -->

<!-- a good guide to "audit" an existing project's design: -->
<!-- https://bluesock.org/~willkg/blog/dev/auditing_projects.html -->
<!-- the following sections are partially based on that -->

## Installation

the `profile::weather` class in the `tor-puppet` repository configures a systemd service to run the `tor_weather` app with [gunicorn][], as well as an apache site config to proxy requests to `http://localhost:8000`. tor-weather handles its own database schema creation, but database and database user creation are still manual.

add the `profile::weather` class to a node in puppet, then follow the instructions below to configure and deploy the application.

[gunicorn]: <https://gunicorn.org/>

### Creating the postgres database

First, follow the [postgresql installation howto][].

Run `sudo -u postgres psql` and enter the following commands. Make sure you generate a secure password for the torweather user. The password must be url safe (ASCII alphanumeric characters, `-`, `_`, `~`) since we'll be using the password in a URI later.

```
CREATE DATABASE torweather;
CREATE USER torweather;
alter user torweather password '<password>';
GRANT ALL ON DATABASE torweather TO torweather;
```

[postgresql installation howto]: <https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/postgresql#installation>

### Preparing a release

because tor-weather is managed using [poetry][], there are a few steps necessary to prepare a release before deploying:

1. clone the [tor-weather repo][] locally
1. export the dependencies using poetry: `poetry export --output requirements.txt`
1. note which dependencies are installable by apt using this [list of packages in debian][]
1. check out the latest release, and build a wheel: `poetry build --format=wheel`
1. `scp` the wheel and requirements files to the server: `scp requirements.txt dist/tor_weather-*.whl weather-01.torproject.org:/home/weather/`

[poetry]: <https://python-poetry.org/>
[tor-weather repo]: <https://gitlab.torproject.org/tpo/network-health/tor-weather>
[list of packages in debian]: <https://packages.debian.org/stable/allpackages?format=txt.gz>

### Installing on the server

1. deploy the `role::weather` Puppet class to the server
1. create a virtual environment: `python3 -m venv tor-weather-venv` and source it: `. tor-weather-venv/bin/activate`
1. install the remaining dependencies from requirements.txt: `pip install -r requirements.txt`
1. enable and start the systemd user service units: `tor-weather.service tor-weather-celery.service tor-weather-celerybeat.service`
1. the /home/weather/.tor-weather.env file configures the tor-weather application through environment variables. This file is managed by Puppet.

```
SMTP_HOST=localhost
SMTP_PORT=25
SMTP_USERNAME=weather@torproject.org
SMTP_PASSWORD=''

SQLALCHEMY_DATABASE_URI='postgresql+psycopg2://torweather:<database password>@localhost:5432/torweather'

BROKER_URL='amqp://torweather:<broker password>@localhost:5672'
API_URL='https://onionoo.torproject.org'
BASE_URL='https://weather.torproject.org'

ONIONOO_JOB_INTERVAL=15

# XXX: change this
# EMAIL_ENCRYPT_PASS is a 32 byte string that has been base64-encoded
EMAIL_ENCRYPT_PASS='Q0hBTkdFTUVDSEFOR0VNRUNIQU5HRU1FQ0hBTkdFTUU='

# XXX: change this
SECRET_KEY='secret'

SQLALCHEMY_TRACK_MODIFICATIONS=
CELERY_BIN=/home/weather/tor-weather-venv/bin/celery
CELERY_APP=tor_weather.celery.celery
CELERYD_NODES=worker1
CELERYD_LOG_FILE=logs/celery/%n%I.log
CELERYD_LOG_LEVEL=info
CELERYD_OPTS=
CELERYBEAT_LOG_FILE=logs/celery/beat.log
CELERYBEAT_LOG_LEVEL=info
```

[Creating the postgres database]: <#creating-the-postgres-database>

## Upgrades

1. download a pre-built wheel from the [tor-weather ci][] or build one locally with poetry
1. upload the wheel to the tor-weather machine with `scp tor_weather-0.1.0-py3-none-any.whl weather-01.torproject.prg:~/`
1. activate the tor-weather virtualenv
1. install the wheel: `pip install tor_weather-0.1.0-py3-none-any.whl`
1. restart the service: `sudo -u weather env XDG_RUNTIME_DIR=/run/user/$(id -u weather) systemctl --user restart tor-weather.service`
1. restart the celery service: `sudo -u weather env XDG_RUNTIME_DIR=/run/user/$(id -u weather) systemctl --user restart tor-weather-celery.service`
1. restart the celery beat service: `sudo -u weather env XDG_RUNTIME_DIR=/run/user/$(id -u weather) systemctl --user restart tor-weather-celerybeat.service`

[tor-weather ci]: <https://gitlab.torproject.org/tpo/network-health/tor-weather/-/pipelines>

## Migrating the database schema

After an upgrade or an initial deployment, you'll need to create or migrate the database schema. This script will activate the tor-weather virtual environment, export the tor-weather envvar settings, and then create/migrate the database schema. Note: the `flask` command might need to get updated dependent on the Python version running.

```
sudo -u weather bash
cd /home/weather
source tor-weather-venv/bin/activate
set -a
source .tor-weather.env
set +a
flask --app tor_weather.app db upgrade --directory /home/weather/tor-weather-venv/lib/python3.11/site-packages/tor_weather/migrations
exit
```

## SLA

<!-- this describes an acceptable level of service for this service -->

## Design and architecture

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution" discussed in -->
<!-- a previous RFC or the Discussion section below. it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

## Services

The tor-weather deployment consists of three main services:

1. apache: configured in puppet. proxies requests to `http://localhost:8000`
1. gunicorn: started by a systemd service file configured in puppet. runs with 5 workers (recommended by gunicorn docs: (2 * nproc) + 1), listens on localhost port 8000
1. postgres: a base postgres installation with a `torweather` user and database

Additionally, there are three services related to task scheduling:

1. rabbitmq: configured in puppet, a message broker (listening on `localhost:5672`)
1. celery: task queue, started by a systemd service file configured in puppet
1. celery beat: scheduler, started by a systemd service file configured in puppet

## Storage

tor-weather is backed by a postgres database. the postgres database is configured in the `/home/weather/.tor-weather.env` file, using a [sqlalchemy connection URI][].

[sqlalchemy connection URI]: <https://docs.sqlalchemy.org/en/20/core/engines.html#database-urls>

## Queues

### Onionoo Update Job

The `tor-weather-celerybeat.service` file triggers a job every 15 minutes to update tor-weather's onionoo metrics information.

## Interfaces

<!-- e.g. web APIs, commandline clients, etc -->

## Authentication

tor-weather handles its own user creation and authentication via the web interface.

## Implementation

<!-- programming languages, frameworks, versions, license -->

## Related services

- [Postgres](/howto/postgresql)

## Issues

Issues can be filed on the [tor-weather issue tracker][].

[tor-weather issue tracker]: <https://gitlab.torproject.org/tpo/network-health/tor-weather/-/issues/>

## Maintainer

tor-weather is maintained by the [network-health team][].

[network-health team]: <https://gitlab.torproject.org/tpo/network-health>

## Users

<!-- who the main users are, how they use the service. possibly reuse -->
<!-- the Personas section in the RFC, if available. -->

## Monitoring and metrics

<!-- describe how this service is monitored, how security issues and -->
<!-- upgrades are tracked, see also "Upgrades" above. -->

## Tests

<!-- how the service can be tested, for example after major changes -->
<!-- like IP address changes or upgrades. describe CI, test suites, linting -->

## Logs

<!-- where are the logs? how long are they kept? any PII? -->
<!-- what about performance metrics? same questions -->
Logs are kept in `<working directory>/logs`. In the current deployment this is `/home/weather/tor-weather/logs`.

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

<!-- references to upstream documentation, if relevant -->

# Discussion

<!-- the "discussion" section is where you put any longer conversation -->
<!-- about the project that you will not need in a casual -->
<!-- review. history of the project, why it was done the way it was -->
<!-- (as opposed to how), alternatives, and other proposals are -->
<!-- relevant here. -->

<!-- this at least partly overlaps with the TPA-RFC process (see -->
<!-- policy.md), but in general should defer to proposals when -->
<!-- available -->

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. --> 

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->
