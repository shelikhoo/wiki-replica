The vault service, based on [Vaultwarden][], serves as a secrets storage
application.

[Vaultwarden]: https://github.com/dani-garcia/vaultwarden

[[_TOC_]]

<!-- note: this template was designed based on multiple sources: -->
<!-- https://diataxis.fr/ -->
<!-- http://opsreportcard.com/section/9-->
<!-- http://opsreportcard.com/section/11 -->
<!-- comments like this one should be removed on instantiation -->

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Pager playbook

<!-- information about common errors from the monitoring system and -->
<!-- how to deal with them. this should be easy to follow: think of -->
<!-- your future self, in a stressful situation, tired and hungry. -->

## Disaster recovery

<!-- what to do if all goes to hell. e.g. restore from backups? -->
<!-- rebuild from scratch? not necessarily those procedures (e.g. see -->
<!-- "Installation" below but some pointers. -->

# Reference

<!-- this section is a more in-depth review of how this service works, -->
<!-- how it's setup. day-to-day operation should be covered in -->
<!-- tutorial or how-to, this is more in-depth -->

<!-- a good guide to "audit" an existing project's design: -->
<!-- https://bluesock.org/~willkg/blog/dev/auditing_projects.html -->
<!-- the following sections are partially based on that -->

## Installation

This service is installed using the upstream-provided container which runs under
[Podman][].

To set it up, deploy the `profile::vaultwarden` Puppet profile. This will:

- install Podman
- deploy an unprivileged user/group pair
- manage this user's home directory under `/srv/vault.torproject.org`
- install systemd unit to instantiate and manage the container
- install the container configuration in
  `/srv/vault.torproject.org/container-env`
- create a directory for the container's persistent storage in
  `/srv/vault.torproject.org/data`
- deploy a cron job to create a database backup

The installation requirements are recorded in the GitLab ticket
[tpo/tpa/team#41541](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41541).

[Podman]: https://podman.io/

### Manual

This procedure documents a manual installation performed in a lab, for
testing purposes. It was also done manually because the environment is
different than production (Apache vs Nginx, Docker vs Podman).

 1. create system user
 
        addgroup --system vaultwarden
        adduser --system vaultwarden

 2. create a Docker compose file, note how the `user` is numeric
    below, it needs to match the UID and GID created above:

```
version: '3'
services:
  vaultwarden:
    image: vaultwarden/server:latest
    container_name: vaultwarden
    restart: always
    environment:
      DOMAIN: "https://vault.example.com"
      SIGNUPS_ALLOWED: "false"
      ROCKET_ADDRESS: "127.0.0.1"
      ROCKET_PORT: 8086
      IP_HEADER: "X-Forwarded-For"
      SMTP_PORT: 25
      SMTP_HOST: "localhost"
      SMTP_FROM: "vault@example.com"
      HELO_NAME: "vault.example.com"
      SMTP_SECURITY: "off"
    env_file: "admin-token.env"
    volumes:
      - data:/data:Z
    restart: unless-stopped
    network_mode: host
    user: 108:127
volumes:
  data:
```

 3. create the secrets file:

        # generate a strong secret and store it in your password manager
        tr -dc '[:alnum:]' < /dev/urandom | head -c  40
        docker run --rm -it  vaultwarden/server /vaultwarden hash
    
    copy-paste the `ADMIN_TOKEN` line in the `/etc/docker/admin-token.env` file.

 4. start the container, which will fail on a permission issue:
 
        docker-compose up

 5. fix perms:
 
        chown vaultwarden:vaultwarden /var/lib/docker/volumes/vaultwarden_data/_data

 6. start the container properly

        docker-compose up

 7. setup DNS, webserver and TLS, see their [proxy examples](https://github.com/dani-garcia/vaultwarden/wiki/Proxy-examples)

 8. setup backups, upgrades, fail2ban, etc

Assuming you setup the service on the domain `vault.example.com`, head
towards `https://vault.example.com/admin` to access the admin
interface.

## Upgrades

Because the cintainer is started with label `io.containers.autoupdate=registry`
and the systemd unit is configured to create new containers on startup (`--new`
switch on the `podman generate systemd` command) the container will be
auto-upgraded daily from the upstream container registry via the
`podman-auto-update` service/timer unit pair (enabled by default on bookworm).

## SLA

<!-- this describes an acceptable level of service for this service -->

## Design and architecture

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution" discussed in -->
<!-- a previous RFC or the Discussion section below. it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

## Services

The service is set up using a single all-in-one container, pulled from
`quay.io/vaultwarden/server:latest` which listens for HTTP/1.1 connections on
port 8080. The container is started/stopped using the
`container-vaultwarden.service` systemd unit.

An nginx instance is installed in front of port 8080 to proxy connections from
the standard web ports 80 and 443 and handle HTTPS termination.

## Storage

All the Vaultwarden data, including SQlite3 database is stored below
`/srv/vault.torproject.org/data`.

## Interfaces

<!-- e.g. web APIs, commandline clients, etc -->

## Authentication

Vaultwarden has its own user database.

The instance is administered using a secret `ADMIN_TOKEN` which allows service
admins to login at https://vault.torproject.org/admin

## Implementation

<!-- programming languages, frameworks, versions, license -->

## Related services

<!-- dependent services (e.g. authenticates against LDAP, or requires -->
<!-- git pushes)  -->

## Issues

<!-- such projects are never over. add a pointer to well-known issues -->
<!-- and show how to report problems. usually a link to the -->
<!-- issue tracker. consider creating a new Label to regroup the -->
<!-- issues if using the general tracker. see also TPA-RFC-19. -->

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~Foo.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Foo

## Maintainer

<!-- document who deployed and operates this service, the team and -->
<!-- ideally the person inside that team -->

## Users

<!-- who the main users are, how they use the service. possibly reuse -->
<!-- the Personas section in the RFC, if available. -->

## Upstream

<!-- who the upstreams are, if they are still active, -->
<!-- collaborative, how do we keep up to date, support channels, see -->
<!-- also the "Issues" section above -->

## Monitoring and metrics

<!-- describe how this service is monitored, how security issues and -->
<!-- upgrades are tracked, see also "Upgrades" above. -->

## Tests

<!-- how the service can be tested, for example after major changes -->
<!-- like IP address changes or upgrades. describe CI, test suites, linting -->

## Logs

The logs for Vaultwarden can be read using
`journalctl -u container-vaultwarden.service`.

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

<!-- references to upstream documentation, if relevant -->

# Discussion

<!-- the "discussion" section is where you put any longer conversation -->
<!-- about the project that you will not need in a casual -->
<!-- review. history of the project, why it was done the way it was -->
<!-- (as opposed to how), alternatives, and other proposals are -->
<!-- relevant here. -->

<!-- this at least partly overlaps with the TPA-RFC process (see -->
<!-- policy.md), but in general should defer to proposals when -->
<!-- available -->

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. -->

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->
